# 环境部署

## 准备工作
```
JDK >= 1.8 (推荐1.8版本)
Mysql >= 5.5.0 (推荐5.7版本)
Redis >= 3.0
Maven >= 3.0
Node >= 10
```

## 运行系统
前往Gitee下载页面[https://gitee.com/anji-plus/appsp/java](https://gitee.com/anji-plus/appsp/tree/master/java)下载解压到工作目录

## 后端运行
1. 通过Idea或Eclipse导入加载appsp/java Maven依赖包
2. 创建数据库app_sp_service_platform并导入数据脚本app_sp.sql
3. 修改`spring.datasource.druid.xx`数据库连接
4. 修改`spring.redis.xx`redis连接
5. 修改`file.apk.url` apk 下载路径前缀
6. 修改`upload.filename`文件上传路径
7. 打开运行 sp-app moudle下的com.anji.sp.SpAppApplication

## 前端运行
```
# 进入项目目录
cd appsp/web

# 安装依赖
npm install

# 加载依赖项
npm install

# 本地开发 启动项目
npm run dev
```
打开浏览器，输入：http://localhost:80 （默认账户 `admin/ajplus`）

若能正确展示登录页面，并能成功登录，菜单及页面展示正常，则表明环境搭建成功


> 提示
  因为本项目是前后端分离的，所以需要前后端都启动好，才能进行访问

## 必要配置
1. 修改数据库链接<br>
    `sp-app moudle 编辑resources目录下的application-xx.yml`<br>
     `spring.datasource.druid.url: 服务器地址`<br/>
     `spring.datasource.druid.username: 账号`<br/>
     `spring.datasource.druid.password: 密码`<br/>
2. 修改redis
    `sp-app moudle 编辑resources目录下的application-xx.yml`<br/>
         `spring.redis` redis账号及地址<br/>
    如果要配置多节点模式请自行修改`RedissonConfig`配置<br/>
3. apk文件上传及下载<br/>
    修改`file.apk.url` apk 下载路径前缀 <br/>
    修改`upload.filename`文件上传路径<br/>

## 部署系统
> 提示:<br/>因为本项目是前后端分离的，所以需要前后端都部署好，才能进行访问

### 后端部署
`cd ./sp-app `<br/>
`mvn clean install -DskipTests` <br/>
`-DskipTests`，不执行测试用例，但编译测试用例类生成相应的class文件至`target/test-classes`下。<br/>
`-Dmaven.test.skip=true`，不执行测试用例，也不编译测试用例类。<br/>
使用命令行执行：<br/>
`java -jar target/sp-1.0.0.jar --spring.profiles.active=open`
#### 注意
上传文件的目录需要独自配置</br>
例如：目录文件 properties中upload.filename配置/app/file-sp <br/>
服务器将创建文件目录/app/file-sp/apk 供方apk使用 <br/>
file.apk.url为下载地址Nginx配置虚拟下载地址<br/>

#### Nginx配置

```
server {
    listen       80;
    server_name  open-appsp.anji-plus.com;

# 前端Html目录
    location / {
        root   /app/portal-appsp;
        index  index.html index.htm;
        try_files $uri $uri/ /index.html 404;
    }
# 接口地址统一加sp前缀
    location /sp {
        proxy_pass http://10.108.0.1:8081;
    }
# 文件下载地址
    location /download {
        alias /app/file-sp/apk/;
    }

    error_page 404 /404.html;
        location = /40x.html {
    }

    error_page 500 502 503 504 /50x.html;
        location = /50x.html {
    }
}
```


### 前端部署
当项目开发完毕，只需要运行一行命令就可以打包你的应用<br/>
`cd /appsp/web`

```
# 打包正式环境
npm run build:prod

# 打包预发布环境
npm run build:uat
```
构建打包成功之后，会在根目录生成 dist 文件夹，里面就是构建打包好的文件，通常是 ***.js 、***.css、index.html 等静态文件。
通常情况下 dist 文件夹的静态文件发布到你的 nginx 或者静态服务器即可，其中的 index.html 是后台服务的入口页面。

>注意：<br/>config/xxx.env.js是对应环境的接口地址，如果要添加或替换xxx.env.js,需要修改webpack.prod.conf.js的及package.json的scripts

如下：
```
// webpack.prod.conf.js
//如果要修改xxx.env.js中的NODE_ENV和webpack.prod.conf.js是一一对应的
var env = require('../config/dev.env')
if (process.env.NODE_ENV === 'testing') {
  env = require('../config/test.env')
}
if (process.env.NODE_ENV === 'production') {
env = require('../config/prod.env')
}
if (process.env.NODE_ENV === 'uat') {
env = require('../config/uat.env')
}

//package.json
//如果要修改xxx.env.js中的NODE_ENV也需要变动package.json的scripts
"scripts": {
    "dev": "webpack-dev-server --inline --progress --config build/webpack.dev.conf.js",
    "dev:test": "NODE_ENV=testing npm run dev",
    "dev:uat": "NODE_ENV=uat npm run dev",
    "dev:prod": "NODE_ENV=production npm run dev",
    "start": "npm run dev",
    "build": "cross-env NODE_ENV=production node build/build.js",
    "build:dev": "cross-env NODE_ENV=development node build/build.js",
    "build:test": "cross-env NODE_ENV=testing node build/build.js",
    "build:uat": "cross-env NODE_ENV=uat node build/build.js",
    "build:prod": "cross-env NODE_ENV=production node build/build.js"
  }

```

## 常见问题
1. 后端服务部署 `ip2region.db` 需要放到`Nginx`上
然后将 sp-auth 模块下IPUntils.java 中 getCityInfo的dbPath替换成Nginx中ip2region.db的目录
2. 目录文件 `.properties`中`upload.filename`配置`/app/file-sp` <br/>
   服务器将创建文件目录`/app/file-sp/apk` 用来放apk使用 <br/>
   `file.apk.url`为下载地址Nginx配置虚拟下载地址<br/>
3. 行为验证码右下角logo乱码<br/>
    请参考：[AJ-Captcha Wiki](https://gitee.com/anji-plus/captcha/wikis/1.java后端接入-1.2.1版本后?sort_id=2308156) Wiki
