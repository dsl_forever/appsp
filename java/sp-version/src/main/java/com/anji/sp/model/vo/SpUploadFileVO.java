package com.anji.sp.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 版本管理
 *
 * @author kean 2020-06-26
 */
@Data
@ApiModel("文件上传结果VO")
public class SpUploadFileVO implements Serializable {

    @ApiModelProperty(value = "版本名称", required = true)
    private String versionName;

    @ApiModelProperty(value = "版本号", required = true)
    private String versionNumber;

    /** 文件性一id */
    @ApiModelProperty("文件性一id")
    private String fileId;

    /** 文件在linux中的完整目录，比如/app/file-sp/file/${fileid}.xlsx */
    @ApiModelProperty("文件在linux中的完整目录")
    private String filePath;

    /** 通过接口的下载完整http路径 */
    @ApiModelProperty("通过接口的下载完整http路径")
    private String urlPath;

}