package com.anji.plus.ajpushlibrary.vivo;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.anji.plus.ajpushlibrary.AppSpPushConfig;
import com.anji.plus.ajpushlibrary.AppSpPushLog;
import com.anji.plus.ajpushlibrary.AppSpPushConstant;
import com.anji.plus.ajpushlibrary.model.AppSpModel;
import com.anji.plus.ajpushlibrary.service.IAppSpCallback;
import com.vivo.push.IPushActionListener;
import com.vivo.push.PushClient;

public class VivoPushUtil {
    private Context context;

    public VivoPushUtil(Context context) {
        this.context = context;
    }

    //用于启动打开push开关
    public void bind() {
        PushClient.getInstance(context).turnOnPush(new IPushActionListener() {

            @Override
            public void onStateChanged(int state) {
                if (state != 0) {
                    AppSpPushLog.e("打开push异常" + state);
                } else {
                    AppSpPushLog.e("打开push成功" + state);

                    AppSpPushConstant.pushToken = PushClient.getInstance(context).getRegId();

                    Log.i("vivo的id",PushClient.getInstance(context).getRegId());



                    AppSpPushConfig.getInstance().sendRegTokenToServer(new IAppSpCallback() {
                        @Override
                        public void pushInfo(AppSpModel<String> appSpModel) {

                        }

                        @Override
                        public void error(String code, String msg) {

                        }
                    });
                }
            }
        });

    }

    public void unbind(View v) {
        PushClient.getInstance(context).turnOffPush(new IPushActionListener() {

            @Override
            public void onStateChanged(int state) {
                if (state != 0) {
                    AppSpPushLog.e("关闭push异常" + state);
                } else {
                    AppSpPushLog.e("关闭push成功" + state);
                }
            }
        });
    }

    // 删除别名
    public void delAlias(String alias) {
        PushClient.getInstance(context).unBindAlias(alias, new IPushActionListener() {

            @Override
            public void onStateChanged(int state) {
                if (state != 0) {
                    AppSpPushLog.e("取消别名异常" + state);
                } else {
                    AppSpPushLog.e("取消别名成功" + state);
                }
            }
        });
    }

    // 设置别名
    public void setAlias(String alias) {
        PushClient.getInstance(context).bindAlias(alias, new IPushActionListener() {

            @Override
            public void onStateChanged(int state) {
                if (state != 0) {
                    AppSpPushLog.e("设置别名异常" + state);
                } else {
                    AppSpPushLog.e("设置别名成功" + state);
                }
            }
        });
    }

    // 设置标签
    public void setTopic(String topic) {
        PushClient.getInstance(context).setTopic(topic, new IPushActionListener() {

            @Override
            public void onStateChanged(int state) {
                if (state != 0) {
                    AppSpPushLog.e("设置标签异常" + state);
                } else {
                    AppSpPushLog.e("设置标签成功" + state);
                }
            }
        });
    }

    // 删除标签
    public void delTopic(String topic) {
        // Push: 删除标签调用方式
        PushClient.getInstance(context).delTopic(topic, new IPushActionListener() {

            @Override
            public void onStateChanged(int state) {
                if (state != 0) {
                    AppSpPushLog.e("删除标签异常" + state);
                } else {
                    AppSpPushLog.e("删除标签成功" + state);
                }
            }
        });
    }

}


