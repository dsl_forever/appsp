//
//  DeviceInfo.h
//  AJPushSDK
//
//  Created by Black on 2021/2/2.
//  Copyright © 2018 anji-plus 安吉加加信息技术有限公司 http://www.anji-plus.com. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AJPushDeviceInfo : NSObject
//获取手机型号
+ (NSString *)getBrandInfo;
//获取手机系统版本
+ (NSString *)getOSVerison;
@end

NS_ASSUME_NONNULL_END
